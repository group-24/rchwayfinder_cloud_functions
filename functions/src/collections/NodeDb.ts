import { db } from '../collections/db';
import { nodeCollection } from '../data_types/constants';

export class NodeDb {

    addNode = (nodeId, node) => {
        return db.collection(nodeCollection).doc(nodeId).set(node.toJSONObject());
    };

    getNodeById = (nodeId) => {
        return db.collection(nodeCollection).doc(nodeId).get()
    };

    removeNodeById = (nodeId) => {
        return db.collection(nodeCollection).doc(nodeId).delete()
    };

    updateNodeById = (nodeId, node) => {
      return db.collection(nodeCollection).doc(nodeId).update(node);
    };

    addNodes = (newNodes) => {
        const batch = db.batch();

        for(const node of newNodes) {
            const ref = db.collection(nodeCollection).doc(node.nodeId);
            batch.set(ref, node);
        }

        return batch.commit();
    }

    updateNodes = (updatedNodes) => {
        const batch = db.batch();

        for(const node of updatedNodes) {
            const ref = db.collection(nodeCollection).doc(node.nodeId);
            batch.set(ref, node);
        }

        return batch.commit();
    }

    removeNodes = (nodes) => {
        const batch = db.batch();

        for(const node of nodes) {
            const ref = db.collection(nodeCollection).doc(node);
            batch.delete(ref);
        }
        return batch.commit();

    }

    getNodesByFloorId = (floorId) => {
        return db.collection(nodeCollection).where("floorId", "==", floorId).get();
    }
}

