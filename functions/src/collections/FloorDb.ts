import { db } from '../collections/db';
import { floorCollection } from '../data_types/constants';
export class FloorDb {

    addFloor = (floorId,floor) => {
        return db.collection(floorCollection).doc(floorId).set(floor.toJSONObject());    
    };

    getFloorById = (floorId) => {
        return db.collection(floorCollection).doc(floorId).get()
    }

    removeFloorById = (floorId) => {
        return db.collection(floorCollection).doc(floorId).delete()
    }

    updateFloorById = (floorId, floor) => {
        return db.collection(floorCollection).doc(floorId).update(floor);
    }

    getNodesByBuildingId = (buildingId) => {
        return db.collection(floorCollection).where("buildingId", "==", buildingId).get();
    }
}

       