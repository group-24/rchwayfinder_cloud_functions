import { db } from '../collections/db';
import { edgeCollection } from '../data_types/constants';

export class EdgeDb {

    addEdge = (edgeId, edge) => {
        //return db.collection(edgeCollection).add(edge.toJSONObject())
        return db.collection(edgeCollection).doc(edgeId).set(edge);
    };

    getEdgeById = (edgeId) => {
        return db.collection(edgeCollection).doc(edgeId).get()
    }

    removeEdgeById = (edgeId) => {
        return db.collection(edgeCollection).doc(edgeId).delete()
    }

    updateEdgeById = (edgeId, edge) => {
        return db.collection(edgeCollection).doc(edgeId).update(edge);
    }

    addEdges = (newEdges) => {
        const batch = db.batch();

        for(const edge of newEdges) {
            const ref = db.collection(edgeCollection).doc(edge.edgeId);
            batch.set(ref, edge);
        }

        return batch.commit();
    }

    updateEdges = (updatedEdges) => {
        const batch = db.batch();

        for(const edge of updatedEdges) {
            const ref = db.collection(edgeCollection).doc(edge.edgeId);
            batch.set(ref, edge);
        }
        return batch.commit();
    }

    removeEdges = (edges) => {
        const batch = db.batch();

        for(const edge of edges) {
            const ref = db.collection(edgeCollection).doc(edge);
            batch.delete(ref);
        }
        return batch.commit();

    }

    getEdgesByFloorId = (floorId) => {
        return db.collection(edgeCollection).where("floorId", "==", floorId).get();
    }
}

       