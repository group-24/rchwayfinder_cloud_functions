import { db } from '../collections/db';
import {buildingCollection, edgeCollection, floorCollection, nodeCollection} from '../data_types/constants';
import {Floor} from '../data_types/Floor';
export class BuildingDb {

    addBuilding = (buildingId, building) => {   
        const batch = db.batch();
        const buildingRef = db.collection(buildingCollection).doc(buildingId);
        const newBuilding = building.toJSONObject();
        console.log(building.toJSONObject());
        batch.set(buildingRef,newBuilding); 
        for(let i = 0; i < building.floorCount; i++) {
            const newFloor = new Floor();
            const newId = this.guid();
            newFloor.buildingId = buildingId;
            newFloor.floorId = newId;
            newFloor.floorName = i.toString();
            newFloor.floorImgUrl ="";
            newFloor.floorOrder = i;
            const newFloorJSON = newFloor.toJSONObject();
            const ref = db.collection(floorCollection).doc(newId);
            batch.set(ref,newFloorJSON);
        }
        return batch.commit();
    };

    getBuildingById = (buildingId) => {
        return db.collection(buildingCollection).doc(buildingId).get()
    }

    removeBuildingById = (buildingId) => {
        let batch = db.batch();
        return db.collection(nodeCollection).where("buildingId", "==", buildingId)
            .get()
            .then(querySnapshot => {
                querySnapshot.forEach(doc => {
                    batch.delete(doc.ref);
                });

                return null;
            })
            .then(() => {
                return db.collection(edgeCollection).where("buildingId", "==", buildingId)
                    .get();
            })
            .then(snapshot => {
                snapshot.forEach(doc => {
                    batch.delete(doc.ref);
                });
                return null;
            })
            .then(() => {
                return db.collection(floorCollection).where("buildingId", "==", buildingId)
                    .get();
            })
            .then(snapshot => {
                snapshot.forEach(doc => {
                    batch.delete(doc.ref);
                });
                return null;
            })
            .then(() => {
                return db.collection(buildingCollection).doc(buildingId).get();
            })
            .then(snap => {
                batch.delete(snap.ref);
                return null;
            })
            .then(() => {
                return batch.commit();
            })
            .then(() => {
                console.log("Success");
                return null;
            })
            .catch(err => {
                console.log(err);
            });

    }

    updateBuildingById = (buildingId, building) => {
        return db.collection(buildingCollection).doc(buildingId).update(building);
    }

    getBuildings = () => {
        return db.collection(buildingCollection).get();
    }

    private guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
}

       