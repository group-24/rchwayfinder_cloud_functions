import {Building} from '../data_types/Building';
import {BuildingDb} from '../collections/BuildingDb';

// cleans and validates request body and populate Edge object to 
// be sent to database 
export class BuildingService {
    
    addBuilding = (requestBody) => {
        const buildingDb = new BuildingDb();
        //validate edge here before passing down to db
        const newBuilding = this.populateBuilding(requestBody);
        return buildingDb.addBuilding(requestBody.buildingId, newBuilding);
    }

    getBuildingById = (buildingId) => {
        const buildingDb = new BuildingDb();
        return buildingDb.getBuildingById(buildingId);
    }

    removeBuildingById = (floorId) => {
        const buildingDb = new BuildingDb();
        return buildingDb.removeBuildingById(floorId);
    }

    updateBuildingById = (floorId, requestBody) => {
        const buildingDb = new BuildingDb();
        const updatedBuilding = this.populateUpdatedBuilding(requestBody);
        return buildingDb.updateBuildingById(floorId,updatedBuilding);
    }

    getBuildings = () => {
        const buildingDb = new BuildingDb();
        return buildingDb.getBuildings();
    }

    private populateUpdatedBuilding = (requestBody) => {
        const updatedBuildingJSON = {};
        if(requestBody.buildingName) {
            updatedBuildingJSON["buildingName"] = requestBody.buildingName
        }
        if(requestBody.floorCount) {
            updatedBuildingJSON["floorCount"] = requestBody.floorCount
        }
        return updatedBuildingJSON;
    }

    private populateBuilding = (requestBody) => {
        const newBuilding = new Building;
        newBuilding.buildingName = requestBody.buildingName;
        newBuilding.floorCount = requestBody.floorCount;
        newBuilding.buildingId = requestBody.buildingId;
        return newBuilding;
    }
}
