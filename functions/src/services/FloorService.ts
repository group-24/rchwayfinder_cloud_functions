import {Floor} from '../data_types/Floor';
import {FloorDb} from '../collections/FloorDb';

// cleans and validates request body and populate Edge object to 
// be sent to database 
export class FloorService {
    
    addFloor = (requestBody) => {
        const floorDb = new FloorDb();
        //validate edge here before passing down to db
        const newFloor = this.populateFloor(requestBody);
        return floorDb.addFloor(requestBody.floorId, newFloor);
    }

    getFloorById = (floorId) => {
        const floorDb = new FloorDb();
        return floorDb.getFloorById(floorId);
    }

    removeFloorById = (floorId) => {
        const floorDb = new FloorDb();
        return floorDb.removeFloorById(floorId);
    }

    updateFloorById = (floorId, requestBody) => {
        const floorDb = new FloorDb();
        const updatedFloor = this.populateUpdatedFloor(requestBody);
        return floorDb.updateFloorById(floorId,updatedFloor);
    }

    getFloorsByBuildingId = (buildingId) => {
        const floorDb = new FloorDb();
        return floorDb.getNodesByBuildingId(buildingId);
    }

    private populateUpdatedFloor = (requestBody) => {
        const updatedFloorJSON = {};
        if(requestBody.buildingId) {
            updatedFloorJSON["buildingId"] = requestBody.buildingId
        }
        if(requestBody.floorName) {
            updatedFloorJSON["floorName"] = requestBody.floorName
        }
        if(requestBody.floorImgUrl) {
            updatedFloorJSON["floorImgUrl"] = requestBody.floorImgUrl
        }
        return updatedFloorJSON;
    }

    private populateFloor = (requestBody) => {
        const newFloor = new Floor;
        newFloor.buildingId = requestBody.buildingId;
        newFloor.floorName = requestBody.floorName;
        newFloor.floorImgUrl = requestBody.floorImgUrl;
        newFloor.floorOrder = requestBody.floorOrder;
        
        return newFloor;
    }
}
