import {Edge} from '../data_types/Edge';
import {EdgeDb} from '../collections/EdgeDb';

// cleans and validates request body and populate Edge object to 
// be sent to database 
export class EdgeService {
    
    addEdge = (requestBody) => {
        const edgeDb = new EdgeDb();
        const newEdge = this.populateUpdatedEdge(requestBody);
        return edgeDb.addEdge(requestBody.edgeId, newEdge);
    }

    getEdgeById = (edgeId) => {
        const edgeDb = new EdgeDb();
        return edgeDb.getEdgeById(edgeId);
    }

    removeEdgeById = (edgeId) => {
        const edgeDb = new EdgeDb();
        return edgeDb.removeEdgeById(edgeId);
    }

    updateEdgeById = (edgeId, requestBody) => {
        const edgeDb = new EdgeDb();
        const updatedEdge = this.populateUpdatedEdge(requestBody);
        return edgeDb.updateEdgeById(edgeId,updatedEdge);
    }

    addEdges = (requestBody) => {
        const edgeDb = new EdgeDb();
        const newEdges = [];
        for (const edge of requestBody.edges) {
            const e = this.populateUpdatedEdge(edge);
            newEdges.push(e);
        }
        return edgeDb.addEdges(newEdges);

    }

    updateEdges = (requestBody) => {
        const edgeDb = new EdgeDb();
        const updatedEdges = [];
        for (const edge of requestBody.edges) {
            const e = this.populateUpdatedEdge(edge);
            updatedEdges.push(e);
        }
        return edgeDb.updateEdges(updatedEdges);
    }

    removeEdges = (requestBody) => {
        const edgeDb = new EdgeDb();
        return edgeDb.removeEdges(requestBody.edges);
    }

    getEdgesByFloorId = (floorId) => {
        const edgeDb = new EdgeDb();
        return edgeDb.getEdgesByFloorId(floorId);
    }

    private populateUpdatedEdge = (requestBody) => {
        const updatedEdgeJSON = {};
        if(requestBody.connectedNodes) {
            updatedEdgeJSON["connectedNodes"] = requestBody.connectedNodes
        }
        if(requestBody.floorId) {
            updatedEdgeJSON["floorId"] = requestBody.floorId
        }
        if(requestBody.edgeId) {
            updatedEdgeJSON["edgeId"] = requestBody.edgeId;
        }
        if(requestBody.type) {
            updatedEdgeJSON["type"] = requestBody.type;
        }
        if(requestBody.source) {
            updatedEdgeJSON["source"] = requestBody.source;
        }
        if(requestBody.target) {
            updatedEdgeJSON["target"] = requestBody.target;
        }
        if(requestBody.buildingId) {
            updatedEdgeJSON["buildingId"] = requestBody.buildingId;
        }
        return updatedEdgeJSON;
    }


    private populateEdge = (requestBody) => {
        const newEdge = new Edge;
        if(requestBody.connectedNodes) {
            newEdge.connectedNodes = requestBody.connectedNodes;
        }
        if(requestBody.floorId) {
            newEdge.floorId = requestBody.floorId;
        }
        if(requestBody.edgeId) {
            newEdge.edgeId = requestBody.edgeId;
        }
        if(requestBody.type) {
            newEdge.type = requestBody.type;
        }
        if(requestBody.source) {
            newEdge.source = requestBody.source;
        }
        if(requestBody.target) {
            newEdge.target = requestBody.target;
        }
        if(requestBody.buildingId) {
            newEdge.buildingId = requestBody.buildingId;
        }

        return newEdge;
    }
}
