import { Node } from '../data_types/Node';
import { NodeDb } from '../collections/NodeDb';

export class NodeService {

    addNode = (requestBody) => {
        const nodeDb = new NodeDb();
        const newNode = this.populateNode(requestBody);
        return nodeDb.addNode(requestBody.nodeId, newNode);
    };

    getNodeById = (nodeId) => {
        const nodeDb = new NodeDb();

        return nodeDb.getNodeById(nodeId);
    };

    removeNodeById = (nodeId) => {
        const nodeDb = new NodeDb();
        return nodeDb.removeNodeById(nodeId);
    };

    updateNodeById = (nodeId, requestBody) => {
        const nodeDb = new NodeDb();
        const updatedNode = this.populateUpdatedNode(requestBody);
        return nodeDb.updateNodeById(nodeId, updatedNode);
    };

    addNodes = (requestBody) => {
        const nodeDb = new NodeDb();
        const newNodes = [];
        for (const node of requestBody.nodes) {
            const n = this.populateUpdatedNode(node);
            newNodes.push(n);
        }
        return nodeDb.addNodes(newNodes);
    }

    updateNodes = (requestBody) => {
        const nodeDb = new NodeDb();
        const updatedNodes = [];
        for (const node of requestBody.nodes) {
            const n = this.populateUpdatedNode(node);
            updatedNodes.push(n);
        }
        return nodeDb.updateNodes(updatedNodes);

    }

    removeNodes = (requestBody) => {
        const nodeDb = new NodeDb();
        return nodeDb.removeNodes(requestBody.nodes);
    }

    getNodesByFloorId = (floorId) => {
        const nodeDb = new NodeDb();
        return nodeDb.getNodesByFloorId(floorId);
    }

    private populateNode = (requestBody) => {
        return new Node(requestBody.name, requestBody.x, requestBody.y, requestBody.floorId, requestBody.type,
            requestBody.subType, requestBody.adjacentNodes, requestBody.nodeId, requestBody.id, requestBody.buildingId);
    };

    private populateUpdatedNode = (requestBody) => {
        const nodeUpdateObject  = {};

        if (requestBody.name) {
            nodeUpdateObject["name"] = requestBody.name;
        }
        if (requestBody.x) {
            nodeUpdateObject["x"] = requestBody.x;
        }
        if (requestBody.y) {
            nodeUpdateObject["y"] = requestBody.y;
        }
        if (requestBody.floorId) {
            nodeUpdateObject["floorId"] = requestBody.floorId;
        }
        if (requestBody.type) {
            nodeUpdateObject["type"] = requestBody.type;
        }
        if (requestBody.subType) {
            nodeUpdateObject["subType"] = requestBody.subType;
        }
        if (requestBody.adjacentNodes) {
            nodeUpdateObject["adjacentNodes"] = requestBody.adjacentNodes;
        }
        if (requestBody.nodeId) {
            nodeUpdateObject["nodeId"] = requestBody.nodeId;
        }
        if (requestBody.id) {
            nodeUpdateObject["id"] = requestBody.id;
        }
        if (requestBody.buildingId) {
            nodeUpdateObject["buildingId"] = requestBody.buildingId;
        }

        return nodeUpdateObject;
    };
}