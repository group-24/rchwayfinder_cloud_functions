import * as admin from 'firebase-admin';
import 'firebase-functions';
admin.initializeApp();

const settings = {
    timestampsInSnapshots: true
}
admin.firestore().settings(settings);
export default admin.firestore();