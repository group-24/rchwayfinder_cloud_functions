import { https } from 'firebase-functions';
import {EdgeService} from '../services/EdgeService';
const cors = require('cors')({
    origin: true,
});

// POST/addEdge
// Adds a new edge document in Edge collection in firestore
//
// Headers: Content-Type: application/json
//
// Request Body: 
//      {"isOn":false, 
//      "floorId": "996313f9-bcba-4ce4-9f3e-ec90c145439f",
//      "adjacentNodes":["994898bf-6fc9-467c-8af3-75f33a0b544c"]}
//
export const addEdge = 
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const edgeService = new EdgeService();
            const promises = [];
            const p = edgeService.addEdge(request.body)
            promises.push(p);

            Promise.all(promises)
                .then((edgeCreated)=> {
                    response.status(201).json({edgeId: request.body.edgeId}).end();
                })
                .catch(error => {
                    console.log(error)
                    response.status(500).send(error)
                })
        })

    });

// GET/getEdgeById
// Retrieves an Edge document from firestore by edgeId
//
// Headers: Content-Type: application/json
//          edgeId: kCYKeBV7ja5GPKjGh6jk
//
export const getEdgeById =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const edgeId: string = request.get('edgeId');
            // get promise

            const edgeService = new EdgeService();
            const promises = [];
            const p = edgeService.getEdgeById(edgeId)
            promises.push(p);

            Promise.all(promises)
                .then((edgeDocRef) =>{
                    const edge = edgeDocRef[0].data();
                    if (edge === null) {
                        response.status(404).end();
                    }
                    response.status(200).json(edgeDocRef[0].data()).end();
                })
                .catch(error => {
                    console.log(error)
                    response.status(500).send(error)
                })
        })

    });

// DELETE/removeEdgeById
// Deletes an Edge document from firestore by edgeId
//
// Headers: Content-Type: application/json
//          edgeId: kCYKeBV7ja5GPKjGh6jk
//
export const removeEdgeById =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const edgeId: string =request.get('edgeId');

            const edgeService = new EdgeService();
            const promises = [];
            const p = edgeService.removeEdgeById(edgeId)
            promises.push(p);

            Promise.all(promises)
                .then( (ref)=> {
                    // need to check what is being returned and
                    //differentiate if id not found
                    response.status(200).json().end();
                })
                .catch(error=>{
                    console.log(error)
                    response.status(500).send(error)
                })
        })
    })

export const updateEdgeById =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const edgeId: string = request.get('edgeId');

            const edgeService = new EdgeService();
            const promises = [];
            const p = edgeService.updateEdgeById(edgeId, request.body);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    response.status(200).json().end();
                })
                .catch(error=>{
                    console.log(error)
                    response.status(500).send(error)
                })
        })

    })

export const addEdges =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const edgeService = new EdgeService();
            const promises = [];
            const p = edgeService.addEdges(request.body);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    response.status(200).json().end();
                })
                .catch(error=>{
                    console.log(error);
                    response.status(500).send(error);
                })
        })
    })

export const updateEdges =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const edgeService = new EdgeService();
            const promises = [];
            const p = edgeService.updateEdges(request.body);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    response.status(200).json().end();
                })
                .catch(error=>{
                    console.log(error);
                    response.status(500).send(error);
            })
        })
    })

export const removeEdges =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const edgeService = new EdgeService();
            const promises = [];
            const p = edgeService.removeEdges(request.body);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    response.status(200).json().end();
                })
                .catch(error=> {
                    console.log(error);
                    response.status(500).send(error);
                })
        })

    })

export const getEdgesByFloorId =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const floorId: string = request.get('floorId');
            const edgeService = new EdgeService();
            const p = edgeService.getEdgesByFloorId(floorId);

            p.then( (querySnapshot) => {
                const returnEdges = [];
                querySnapshot.forEach( (doc) => {
                    returnEdges.push(doc.data());
                })
                response.status(200).json({edges: returnEdges}).end();
            })
                .catch(error=> {
                    console.log(error);
                    response.status(500).send(error);
                })
        })

    })
