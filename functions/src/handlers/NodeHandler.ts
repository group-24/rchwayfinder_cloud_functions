import { https } from 'firebase-functions';
import { NodeService } from '../services/NodeService';

const cors = require('cors')({
    origin: true,
});

export const addNode =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const nodeService = new NodeService();
            const promises = [];
            const p = nodeService.addNode(request.body);
            promises.push(p);

            Promise.all(promises)
                .then((nodeCreated)=> {
                    response.status(201).json({nodeId: request.body.nodeId}).end();
                })
                .catch(error => {
                    console.log(error);
                    response.status(500).send(error);
                })
        })
    });


export const removeNodeById =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const nodeID: string = request.get('nodeId');

            const nodeService = new NodeService();
            const promises = [];
            const p = nodeService.removeNodeById(nodeID);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    response.status(200).json().end();
                })
                .catch(error=> {
                    console.log(error);
                    response.status(500).send(error);
                })
        })

    });

export const getNodeById =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const nodeId: string = request.get('nodeId');
            // get promise

            const nodeService = new NodeService();
            const promises = [];
            const p = nodeService.getNodeById(nodeId);
            promises.push(p);

            Promise.all(promises)
                .then((nodes) =>{
                    const node = nodes[0].data();
                    if (node === null) {
                        response.status(404).end();
                    }
                    response.status(200).json(nodes[0].data()).end();
                })
                .catch(error => {
                    console.log(error)
                    response.status(500).send(error)
                })
        })

    });

export const updateNodeById =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const nodeId: string = request.get('nodeId');

            const nodeService = new NodeService();
            const promises = [];
            const p = nodeService.updateNodeById(nodeId, request.body);
            promises.push(p);

            Promise.all(promises)
                .then( () => {
                    response.status(200).json().end();
                })
                .catch(error=>{
                    console.log(error);
                    response.status(500).send(error);
                })
        })

    });

export const addNodes =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const nodeService = new NodeService();
            const promises = [];
            const p = nodeService.addNodes(request.body);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    response.status(200).json().end();
                })
                .catch(error=>{
                    console.log(error);
                    response.status(500).send(error);
                })
        })

    })

export const updateNodes =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const nodeService = new NodeService();
            const promises = [];
            const p = nodeService.updateNodes(request.body);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    // return array of ids
                    response.status(200).json().end();
                })
                .catch(error=>{
                    console.log(error);
                    response.status(500).send(error);
                })
        })


    })

export const removeNodes =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const nodeService = new NodeService();
            const promises = [];
            const p = nodeService.removeNodes(request.body);
            promises.push(p);

            Promise.all(promises)
                .then( ()=> {
                    response.status(200).json().end();
                })
                .catch(error=> {
                    console.log(error);
                    response.status(500).send(error);
                })
        })
    })

export const getNodesByFloorId =
    https.onRequest((request, response) => {
        cors(request, response, ()=> {
            const floorId: string = request.get('floorId');
            const nodeService = new NodeService();
            const p = nodeService.getNodesByFloorId(floorId);

            p.then( (querySnapshot) => {
                const returnNodes = [];
                querySnapshot.forEach( (doc) => {
                    returnNodes.push(doc.data());
                })
                response.status(200).json({nodes: returnNodes}).end();
            })
                .catch(error=> {
                    console.log(error);
                    response.status(500).send(error);
                })
        })

    })
