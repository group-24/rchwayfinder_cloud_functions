import { https } from 'firebase-functions';
import { BuildingService } from '../services/BuildingService';

const cors = require('cors')({
    origin: true,
});

export const addBuilding =
    https.onRequest((request, response) => {
        cors(request, response, () => {
        const buildingService = new BuildingService();
        const promises = [];
        const p = buildingService.addBuilding(request.body)
        promises.push(p);

        Promise.all(promises)
            .then((buildingCreated) => {
                response.status(201).json({ buildingId: buildingCreated[0].id }).end();
            })
            .catch(error => {
                console.log(error)
                response.status(500).send(error)
            })
        })
    });


export const getBuildingById =
    https.onRequest((request, response) => {
        const buildingId: string = request.get('buildingId');
        // get promise

        const buildingService = new BuildingService();
        const promises = [];
        const p = buildingService.getBuildingById(buildingId)
        promises.push(p);

        Promise.all(promises)
            .then((buildingDocRef) => {
                const building = buildingDocRef[0].data();
                if (building === null) {
                    response.status(404).end();
                }
                response.status(200).json(buildingDocRef[0].data()).end();
            })
            .catch(error => {
                console.log(error)
                response.status(500).send(error)
            })
    });

export let removeBuildingById =
    https.onRequest((request, response) => {
        const buildingId: string = request.get('buildingId');

        const buildingService = new BuildingService();
        const promises = [];
        const p = buildingService.removeBuildingById(buildingId)
        promises.push(p);

        Promise.all(promises)
            .then((ref) => {
                // need to check what is being returned and 
                //differentiate if id not found
                response.status(200).json().end();
            })
            .catch(error => {
                console.log(error)
                response.status(500).send(error)
            })
    })

export let updateBuildingById =
    https.onRequest((request, response) => {
        cors(request, response, () => {
            const buildingId: string = request.get('buildingId');

            const buildingService = new BuildingService();
            const promises = [];
            const p = buildingService.updateBuildingById(buildingId, request.body);
            promises.push(p);

            Promise.all(promises)
                .then(() => {
                    response.status(200).json().end();
                })
                .catch(error => {
                    console.log(error)
                    response.status(500).send(error)
                })
        })
    })

export const getBuildings =
    https.onRequest((request, response) => {
        cors(request, response, () => {

            const buildingService = new BuildingService();
            const p = buildingService.getBuildings();

            p.then((querySnapshot) => {
                const returnNodes = {};
                querySnapshot.forEach((doc) => {
                    returnNodes[doc.id] = doc.data();
                })
                response.status(200).json(returnNodes).end();
            })
                .catch(error => {
                    console.log(error);
                    response.status(500).send(error);
                })
        })
    })
