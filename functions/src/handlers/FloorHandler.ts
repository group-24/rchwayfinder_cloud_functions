import { https } from 'firebase-functions';
import {FloorService} from '../services/FloorService';

const cors = require('cors')({
    origin: true,
});


// POST/addFloor
// Adds a new floor document in Floor collection in firestore
//
// Headers: Content-Type: application/json
//
// Request Body: 
//      {}
//
export const addFloor = 
    https.onRequest((request, response) => {
        const floorService = new FloorService();
        const promises = [];
        const p = floorService.addFloor(request.body)
        promises.push(p);
        
        Promise.all(promises)
        .then((floorCreated)=> {
            response.status(201).json({floorId: floorCreated[0].id}).end();
        })
        .catch(error => {
            console.log(error)
            response.status(500).send(error)
        })
    });

// GET/getFloorById
// Retrieves an Floor document from firestore by floorId
//
// Headers: Content-Type: application/json
//          floorId: kCYKeBV7ja5GPKjGh6jk
//
export const getFloorById =
    https.onRequest((request, response) => {
        const floorId: string = request.get('floorId');
        // get promise
        
        const floorService = new FloorService();
        const promises = [];
        const p = floorService.getFloorById(floorId)
        promises.push(p);
        
        Promise.all(promises)
        .then((floorDocRef) =>{
            const floor = floorDocRef[0].data();
            if (floor === null) {
                response.status(404).end();
            }
            response.status(200).json(floorDocRef[0].data()).end();
        })
        .catch(error => {
            console.log(error)
            response.status(500).send(error)
        })
    });

// // DELETE/removeFloorById
// // Deletes an Floor document from firestore by floorId
// //
// // Headers: Content-Type: application/json
// //          floorId: kCYKeBV7ja5GPKjGh6jk
// //
export let removeFloorById =
    https.onRequest((request, response) => {
        const floorId: string =request.get('floorId');

        const floorService = new FloorService();
        const promises = [];
        const p = floorService.removeFloorById(floorId)
        promises.push(p);

        Promise.all(promises)
        .then( (ref)=> {
            // need to check what is being returned and 
            //differentiate if id not found
            response.status(200).json().end();
        })
        .catch(error=>{
            console.log(error)
            response.status(500).send(error)
        })
    })

export let updateFloorById =
    https.onRequest((request, response) => {
        cors(request,response,()=> {
            const floorId: string = request.get('floorId');

            const floorService = new FloorService();
            const promises = [];
            const p = floorService.updateFloorById(floorId, request.body);
            promises.push(p);
    
            Promise.all(promises)
            .then( ()=> {
                response.status(200).json().end();
            })
            .catch(error=>{
                console.log(error)
                response.status(500).send(error)
            })
        })
    })

    
export const getFloorsByBuildingId =
https.onRequest((request, response) => {
    cors(request, response, ()=> {
        const buildingId: string = request.get('buildingId');
        const floorService = new FloorService();
        const p = floorService.getFloorsByBuildingId(buildingId);

        p.then( (querySnapshot) => {
            const returnFloors = {};
            querySnapshot.forEach( (doc) => {
                returnFloors[doc.id] = doc.data();
            })
            response.status(200).json(returnFloors).end();
        })
            .catch(error=> {
                console.log(error);
                response.status(500).send(error);
            })
    })

})
