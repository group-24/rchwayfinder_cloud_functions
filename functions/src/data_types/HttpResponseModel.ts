export class HttpResponseModel {
    
    status: number;
    message: string;

    getStatus = () => {
        return this.status;
    }

    getMessage = () => {
        return this.message;
    }

    setStatus = (status) => {
        this.status = status;
    }

    setMessage = (message) => {
        this.message = message;
    }
}