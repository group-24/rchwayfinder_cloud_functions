export class Building {
    
    private _buildingName: string;
    private _floorCount: number;
    private _buildingId: string;

    get buildingId(): string {
        return this._buildingId;
    }
    set buildingId(value: string) {
        this._buildingId = value;
    }

    get buildingName(): string {
        return this._buildingName;
    }
    set buildingName(value: string) {
        this._buildingName = value;
    }
    
    get floorCount(): number {
        return this._floorCount;
    }
     set floorCount(value: number) {
        this._floorCount = value;
    }

    toJSONObject = () => {
        return {
            buildingName: this._buildingName,
            floorCount: this._floorCount,
            buildingId: this._buildingId
        }
    }
}