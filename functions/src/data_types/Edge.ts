export class Edge {

    private _floorId: string;
    private _connectedNodes: string[];
    private _edgeId: string;
    private _type: string;
    private _source: number;
    private _target: number;
    private _buildingId: string;


    get buildingId(): string {
        return this._buildingId;
    }

    set buildingId(value: string) {
        this._buildingId = value;
    }

    get floorId(): string {
        return this._floorId;
    }

    set floorId(value: string) {
        this._floorId = value;
    }

    get connectedNodes(): string[] {
        return this._connectedNodes;
    }

    set connectedNodes(value: string[]) {
        this._connectedNodes = value;
    }

    get edgeId(): string {
        return this._edgeId;
    }

    set edgeId(value: string) {
        this._edgeId = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get source(): number {
        return this._source;
    }

    set source(value: number) {
        this._source = value;
    }

    get target(): number {
        return this._target;
    }

    set target(value: number) {
        this._target = value;
    }

    toJSONObject= () => {
        return {
            floorId : this._floorId,
            connectedNodes : this._connectedNodes

        }
    }

}
