export class Node {


    private _name: string;
    private _x: number;
    private _y: number;
    private _floorId: string;
    private _type: string;
    private _subType: string;
    private _adjacentNodes: string[];
    private _nodeId: string;
    private _id: number;
    private _buildingId: string;

    constructor(name: string, x: number, y: number, floorId: string, type: string, subType: string,
                adjacentNodes: string[], nodeId: string, id: number, buildingId: string) {
        this._name = name;
        this._floorId = floorId;
        this._x = x;
        this._y = y;
        this._type = type;
        this._subType = subType;
        this._adjacentNodes = adjacentNodes;
        this._nodeId = nodeId;
        this._id = id;
        this._buildingId = buildingId;
    }


    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get x(): number {
        return this._x;
    }

    set x(value: number) {
        this._x = value;
    }

    get y(): number {
        return this._y;
    }

    set y(value: number) {
        this._y = value;
    }

    get floorId(): string {
        return this._floorId;
    }

    set floorId(value: string) {
        this._floorId = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get subType(): string {
        return this._subType;
    }

    set subType(value: string) {
        this._subType = value;
    }

    get adjacentNodes() {
        return this._adjacentNodes;
    }

    set adjacentNodes(value: string[]) {
        this._adjacentNodes = value;
    }

    get nodeId() {
        return this._nodeId;
    }

    set nodeId(value: string) {
        this._nodeId = value;
    }

    get id() {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get buildingId(): string {
        return this._buildingId;
    }

    set buildingId(value: string) {
        this._buildingId = value;
    }

    toJSONObject= () => {
        return {
            name : this._name,
            x : this._x,
            y : this._y,
            floorId: this._floorId,
            type: this._type,
            subType: this._subType,
            adjacentNodes: this._adjacentNodes,
            nodeId: this._nodeId,
            id: this._id
        }
    }
}
