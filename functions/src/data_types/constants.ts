export const okHttpStatus : number = 200;
export const createdHttpStatus: number = 201;
export const notFoundHttpStatus : number = 404;
export const serverErrorHttpStatus : number = 500;

export const edgeCollection : string = "edge";
export const nodeCollection : string = "node";
export const floorCollection: string = "floor";
export const buildingCollection: string = "buildings";