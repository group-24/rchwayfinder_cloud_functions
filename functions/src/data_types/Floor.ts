export class Floor {
    
    private _buildingId: string;
    private _floorName: string;
    private _floorImgUrl: string;
    private _floorId: string;
    private _floorOrder: number;

    get floorOrder(): number {
        return this._floorOrder;
    }
    set floorOrder(value: number) {
        this._floorOrder = value;
    }

    get floorId(): string {
        return this._floorId;
    }
    set floorId(value: string) {
        this._floorId = value;
    }

    get buildingId(): string {
        return this._buildingId;
    }

    set buildingId(value: string) {
        this._buildingId = value;
    }

    get floorName(): string {
        return this._floorName;
    }

    set floorName(value: string) {
        this._floorName = value;
    }

    get floorImgUrl(): string {
        return this._floorImgUrl;
    }
    set floorImgUrl(value: string) {
        this._floorImgUrl = value;
    }

    toJSONObject = () => {
        return {
            buildingId: this._buildingId,
            floorName: this._floorName,
            floorImgUrl:this._floorImgUrl,
            floorId:this._floorId,
            floorOrder:this._floorOrder
        }
    }
}